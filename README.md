﻿# Repairer Model

Repairer model for Analytical Models course.

## Changelog

### v1.2

* Downgrade to .NET Framework 4 for Windows XP support

### v1.1

* Fixed major bug in calculations

### v1.0

* Initial release
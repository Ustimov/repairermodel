﻿namespace Repairer.Gui
{
    partial class RepairerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculateButton = new System.Windows.Forms.Button();
            this.inputGroupBox = new System.Windows.Forms.GroupBox();
            this.cTextBox = new System.Windows.Forms.TextBox();
            this.cLabel = new System.Windows.Forms.Label();
            this.nTextBox = new System.Windows.Forms.TextBox();
            this.nLabel = new System.Windows.Forms.Label();
            this.t0TextBox = new System.Windows.Forms.TextBox();
            this.t0Label = new System.Windows.Forms.Label();
            this.tnoTextBox = new System.Windows.Forms.TextBox();
            this.tnoLabel = new System.Windows.Forms.Label();
            this.resultGroupBox = new System.Windows.Forms.GroupBox();
            this.resultRichTextBox = new System.Windows.Forms.RichTextBox();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inputGroupBox.SuspendLayout();
            this.resultGroupBox.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(109, 324);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 0;
            this.calculateButton.Text = "Рассчитать";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.CalculateButton_Click);
            // 
            // inputGroupBox
            // 
            this.inputGroupBox.Controls.Add(this.cTextBox);
            this.inputGroupBox.Controls.Add(this.cLabel);
            this.inputGroupBox.Controls.Add(this.nTextBox);
            this.inputGroupBox.Controls.Add(this.nLabel);
            this.inputGroupBox.Controls.Add(this.t0TextBox);
            this.inputGroupBox.Controls.Add(this.t0Label);
            this.inputGroupBox.Controls.Add(this.tnoTextBox);
            this.inputGroupBox.Controls.Add(this.tnoLabel);
            this.inputGroupBox.Location = new System.Drawing.Point(12, 29);
            this.inputGroupBox.Name = "inputGroupBox";
            this.inputGroupBox.Size = new System.Drawing.Size(272, 82);
            this.inputGroupBox.TabIndex = 1;
            this.inputGroupBox.TabStop = false;
            this.inputGroupBox.Text = "Входные параметры";
            // 
            // cTextBox
            // 
            this.cTextBox.Location = new System.Drawing.Point(215, 37);
            this.cTextBox.Name = "cTextBox";
            this.cTextBox.Size = new System.Drawing.Size(45, 20);
            this.cTextBox.TabIndex = 7;
            // 
            // cLabel
            // 
            this.cLabel.AutoSize = true;
            this.cLabel.Location = new System.Drawing.Point(212, 20);
            this.cLabel.Name = "cLabel";
            this.cLabel.Size = new System.Drawing.Size(14, 13);
            this.cLabel.TabIndex = 6;
            this.cLabel.Text = "C";
            // 
            // nTextBox
            // 
            this.nTextBox.Location = new System.Drawing.Point(148, 37);
            this.nTextBox.Name = "nTextBox";
            this.nTextBox.Size = new System.Drawing.Size(45, 20);
            this.nTextBox.TabIndex = 5;
            // 
            // nLabel
            // 
            this.nLabel.AutoSize = true;
            this.nLabel.Location = new System.Drawing.Point(145, 20);
            this.nLabel.Name = "nLabel";
            this.nLabel.Size = new System.Drawing.Size(15, 13);
            this.nLabel.TabIndex = 4;
            this.nLabel.Text = "N";
            // 
            // t0TextBox
            // 
            this.t0TextBox.Location = new System.Drawing.Point(78, 37);
            this.t0TextBox.Name = "t0TextBox";
            this.t0TextBox.Size = new System.Drawing.Size(45, 20);
            this.t0TextBox.TabIndex = 3;
            // 
            // t0Label
            // 
            this.t0Label.AutoSize = true;
            this.t0Label.Location = new System.Drawing.Point(75, 20);
            this.t0Label.Name = "t0Label";
            this.t0Label.Size = new System.Drawing.Size(16, 13);
            this.t0Label.TabIndex = 2;
            this.t0Label.Text = "t0";
            // 
            // tnoTextBox
            // 
            this.tnoTextBox.Location = new System.Drawing.Point(10, 37);
            this.tnoTextBox.Name = "tnoTextBox";
            this.tnoTextBox.Size = new System.Drawing.Size(45, 20);
            this.tnoTextBox.TabIndex = 1;
            // 
            // tnoLabel
            // 
            this.tnoLabel.AutoSize = true;
            this.tnoLabel.Location = new System.Drawing.Point(7, 20);
            this.tnoLabel.Name = "tnoLabel";
            this.tnoLabel.Size = new System.Drawing.Size(22, 13);
            this.tnoLabel.TabIndex = 0;
            this.tnoLabel.Text = "tно";
            // 
            // resultGroupBox
            // 
            this.resultGroupBox.Controls.Add(this.resultRichTextBox);
            this.resultGroupBox.Location = new System.Drawing.Point(12, 126);
            this.resultGroupBox.Name = "resultGroupBox";
            this.resultGroupBox.Size = new System.Drawing.Size(272, 192);
            this.resultGroupBox.TabIndex = 2;
            this.resultGroupBox.TabStop = false;
            this.resultGroupBox.Text = "Результат";
            // 
            // resultRichTextBox
            // 
            this.resultRichTextBox.Location = new System.Drawing.Point(6, 19);
            this.resultRichTextBox.Name = "resultRichTextBox";
            this.resultRichTextBox.ReadOnly = true;
            this.resultRichTextBox.Size = new System.Drawing.Size(260, 167);
            this.resultRichTextBox.TabIndex = 0;
            this.resultRichTextBox.Text = "";
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(297, 24);
            this.mainMenuStrip.TabIndex = 3;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.helpToolStripMenuItem.Text = "Помощь";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.aboutToolStripMenuItem.Text = "О программе...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // RepairerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 353);
            this.Controls.Add(this.resultGroupBox);
            this.Controls.Add(this.inputGroupBox);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.mainMenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "RepairerForm";
            this.Text = "Модель ремонтника";
            this.inputGroupBox.ResumeLayout(false);
            this.inputGroupBox.PerformLayout();
            this.resultGroupBox.ResumeLayout(false);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.GroupBox inputGroupBox;
        private System.Windows.Forms.TextBox cTextBox;
        private System.Windows.Forms.Label cLabel;
        private System.Windows.Forms.TextBox nTextBox;
        private System.Windows.Forms.Label nLabel;
        private System.Windows.Forms.TextBox t0TextBox;
        private System.Windows.Forms.Label t0Label;
        private System.Windows.Forms.TextBox tnoTextBox;
        private System.Windows.Forms.Label tnoLabel;
        private System.Windows.Forms.GroupBox resultGroupBox;
        private System.Windows.Forms.RichTextBox resultRichTextBox;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}


﻿using Repairer.Library;
using System;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace Repairer.Gui
{
    public partial class RepairerForm : Form
    {
        public RepairerForm()
        {
            InitializeComponent();
        }

        private void CalculateButton_Click(object sender, EventArgs e)
        {
            try
            {
                var tno = Double.Parse(tnoTextBox.Text, NumberStyles.Any, CultureInfo.InvariantCulture);
                var t0 = Double.Parse(t0TextBox.Text, NumberStyles.Any, CultureInfo.InvariantCulture);
                var n = Int32.Parse(nTextBox.Text);
                var c = Int32.Parse(cTextBox.Text);
                var repairer = new RepairerModel(tno, t0, n, c);
                resultRichTextBox.Text = ExtractResultFromModel(repairer);
            }
            catch
            {
                MessageBox.Show(this, "Заданы некорректные входные данные");
            }
        }

        private string ExtractResultFromModel(RepairerModel repairer)
        {
            var stringBuilder = new StringBuilder();
            foreach (var property in repairer.GetType().GetProperties())
            {
                stringBuilder.AppendFormat($"{property.Name} = {property.GetValue(repairer, null)}\n");
            }
            stringBuilder.AppendFormat($"Rho_e / Rho_0 = {repairer.Rho_e/repairer.Rho_0}\n");
            return stringBuilder.ToString();
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog(this);
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿namespace Repairer.Library

open MathNet.Numerics

type RepairerModel(t_no: float, t_0: float, n: int, c: int) = 
    let fact (x: int) = SpecialFunctions.Factorial x
    let mu_no = 1.0 / t_no
    let mu_0 = 1.0 / t_0
    let psi = mu_no / mu_0
    let p_k1 n k p_0 = ((fact n * psi ** float k) / (fact k * fact (n - k))) * p_0
    let p_k2 n k p_0 = ((fact n * psi ** float k) / (float c ** float (k-c) * fact c * fact (n - k))) * p_0 
    let tmp1 = Seq.sum [for k in 0..c -> p_k1 n k 1.0]
    let tmp2 = Seq.sum [for k in c+1..n -> p_k2 n k 1.0]
    let p_0 = (tmp1 + tmp2) ** -1.0
    let q = Seq.sum [for k in c..n -> float (k - c) * p_k2 n k p_0]
    let tmp3 = Seq.sum [for k in 1..c -> float k * p_k1 n k p_0]
    let tmp4 = Seq.sum [for k in c+1..n -> float k * p_k2 n k p_0]
    let l = tmp3 + tmp4
    let u = l - q
    let rho_0 = u / float c
    let n_sr = (float n - l)
    let t_r = (l * t_no) / (float n - l)
    let t_c = t_r + t_no
    let rho_e = t_no / t_c
    let w = t_r - t_0

    member val P_0 = p_0 with get
    member val Q = q with get
    member val L = l with get
    member val U = u with get
    member val Rho_0 = rho_0 with get
    member val n = n_sr with get
    member val Rho_e = rho_e with get
    member val W = w with get
    member val T_r = t_r with get
    member val T_c = t_c with get